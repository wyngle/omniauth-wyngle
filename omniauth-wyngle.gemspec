# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'omniauth/wyngle/version'

Gem::Specification.new do |spec|
  spec.name          = "omniauth-wyngle"
  spec.version       = Omniauth::Wyngle::VERSION
  spec.authors       = ["Panayotis Matsinopoulos"]
  spec.email         = ["panayotis@matsinopoulos.gr"]
  spec.summary       = %q{Wyngle OAuth2 Strategy for OmniAuth}
  spec.homepage      = "https://bitbucket.org/wyngle/omniauth-wyngle"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.7"
  spec.add_development_dependency "rake", "~> 10.0"

  spec.add_runtime_dependency 'omniauth-oauth2'
end
