# OmniAuth Wyngle

Wyngle OAuth2 Strategy for OmniAuth.

Supports the OAuth 2.0 server-side flow. 

## Installing

Add to your `Gemfile`:

```ruby
gem 'omniauth-wyngle'
```

Then `bundle install`.

## Usage

`OmniAuth::Strategies::Wyngle` is simply a Rack middleware. [Read the OmniAuth docs for detailed instruction](https://github.com/intridea/omniauth).

Here's a quick example, adding the middleware to a Rails app in `config/initializers/omniauth.rb`:

```ruby
Rails.application.config.middleware.use OmniAuth::Builder do
  provider :wyngle, ENV['WYNGLE_APP_ID'], ENV['WYNGLE_SECRET']
end
```

[See the example Rails app for full example](https://bitbucket.org/wyngle/ror-eshop).

## Configuring

You can configure several options, which you pass in to the `provider` method via a `Hash`:
```
(... this is under construction ...)
```

## Auth Hash

Here's an example *Auth Hash* available in `request.env['omniauth.auth']`:

```ruby
{
  :provider => 'wyngle',
  :uid => '1234567',
  :info => {
    :email => 'joe@bloggs.com',
    :first_name => 'Joe',
    :last_name => 'Bloggs',
    :gender => 'Male'
  },
  :credentials => {
    :token => 'ABCDEF...', # OAuth 2.0 access_token, which you may wish to store
    :expires_at => 1321747205, # when the access token expires (it always will)
    :expires => true # this will always be true
  }  
}
```
