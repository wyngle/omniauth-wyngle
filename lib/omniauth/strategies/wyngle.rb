require "omniauth/wyngle/version"
require 'omniauth-oauth2'

module OmniAuth
  module Strategies
    class Wyngle < OmniAuth::Strategies::OAuth2
      DEFAULT_SCOPE = "basic_data"

      # change the class name and the :name option to match your application name
      option :name, :wyngle

      option :client_options, {
                                :site => "https://www.wyngle.com"
                            }

      uid { raw_info["id"] }

      info do
        {
            :email => raw_info["email"],
            :gender => raw_info["gender"],
            :first_name => raw_info["first_name"],
            :last_name => raw_info["last_name"],
            :phone => raw_info["phone"],
            :address => raw_info["address"]

        }
      end

      def raw_info
        @raw_info ||= access_token.get('/api/v1/me.json').parsed
      end
    end
  end
end
